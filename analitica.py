# script analitica
#jordan c triana, nueva rama

from pydoc import describe
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 

myData=pd.read_csv("predios_csv_1221.csv", delimiter=';',nrows=1000)   #leer archivo csv 
print(myData.head())     #Cabecera 
print(myData['PreAConst'])  #leer columna 
row=myData.iloc[5]   #fila número 5
print(row) 
print(myData['PreDirecc'])  #leer columna 

print(myData.shape)

#información de las columnas
print(myData.info())

#estadísticas básicas 
print(myData.describe())
print(myData['PreACons'].describe())
print(myData['PreACons'].mean())

#graficar la data 
"""plt.figure()
myData['PreACons'].plot()
plt.show()

plt.figure()
myData['PreACons'].hist()
plt.show()
"""
#subplot
figura,ejes = plt.subplots(nrows=2, ncols=2)
myData['PreACons'].plot(ax=ejes[0,0])
myData['PreACons'].hist(ax=ejes[1,1])
plt.show()